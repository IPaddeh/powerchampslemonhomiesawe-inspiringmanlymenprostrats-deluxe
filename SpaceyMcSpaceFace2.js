//Spacey McSpace Face javascript file

var Spaceface =  function () {
   this.canvas = document.getElementById('game-canvas'),
   this.context = this.canvas.getContext('2d'),

   // HTML elements........................................................
   
   this.fpsElement = document.getElementById('fps'),
   this.toast = document.getElementById('toast'),
   this.scoreElement = document.getElementById('score'),
   this.levelElement = document.getElementById('level'),
   this.lifeElement = document.getElementById('lives'),
   this.pointElement = document.getElementById('points'),

   window.addEventListener('load', function()
    {
 
    var box1 = document.getElementById('game-canvas');
    var statusdiv = document.getElementById('statusdiv');
    var startx = 0;
    var dist = 0;
 
    box1.addEventListener('touchstart', function(e)
    {
        var touchobj = e.changedTouches[0]; // reference first touch point (ie: first finger)
        startx = parseInt(touchobj.clientX); // get x position of touch point relative to left edge of browser
        statusdiv.innerHTML = 'Status: touchstart<br> ClientX: ' + startx + 'px';
        e.preventDefault();
    }, false);
 
    box1.addEventListener('touchmove', function(e)
    {
        var touchobj = e.changedTouches[0]; // reference first touch point for this event
        var dist = parseInt(touchobj.clientX) - startx;
        statusdiv.innerHTML = 'Status: touchmove<br> Horizontal distance traveled: ' + dist + 'px';
        e.preventDefault();
    }, false);
 
    box1.addEventListener('touchend', function(e)
    {
        var touchobj = e.changedTouches[0]; // reference first touch point for this event
        statusdiv.innerHTML = 'Status: touchend<br> Resting x coordinate: ' + touchobj.clientX + 'px';
        e.preventDefault();
    }, false);
 
}, false);

   // Constants............................................................

   this.LEFT = 1,
   this.RIGHT = 2,
   
   this.BACKGROUND_VELOCITY = 35,    // pixels/second
   this.RUN_ANIMATION_RATE = 15,     // frames/second
   
   this.SPACE_JUMP_DURATION = 1000, // milliseconds
   this.SNAIL_PACE_VELOCITY = 50,    // pixels/second
   this.SNAIL_BOMB_VELOCITY = 550,

   
   //Paddy
   this.ROCK_SPARKLE_DURATION = 50, // milliseconds
   this.ROCK_SPARKLE_INTERVAL = 50, // milliseconds
   this.ROCK_BOUNCE_RISE_DURATION = 80, // milliseconds
   
   this.STAR_SPARKLE_DURATION = 100, // milliseconds
   this.STAR_SPARKLE_INTERVAL = 50, // milliseconds
   this.STAR_BOUNCE_RISE_DURATION = 80, // milliseconds
    //Paddy

   this.GRAVITY_FORCE = 9.81,
   this.PIXELS_PER_METER = this.canvas.width / 10; // 10 meters, randomly selected width

   this.PAUSED_CHECK_INTERVAL = 200,
   this.DEFAULT_TOAST_TIME = 3000, // 3 seconds

   this.EXPLOSION_CELLS_HEIGHT = 62,
   this.EXPLOSION_DURATION = 500,

   this.NUM_TRACKS = 3,

   this.PLATFORM_HEIGHT = 8,  
   this.PLATFORM_STROKE_WIDTH = 2,
   this.PLATFORM_STROKE_STYLE = 'rgb(0,0,0)',

   // Platform scrolling offset is PLATFORM_VELOCITY_MULTIPLIER *
   // backgroundOffset. The platforms move PLATFORM_VELOCITY_MULTIPLIER
   // times as fast as the background.
   
   this.SATALITE_CELLS_WIDTH = 135,
   this.SATALITE_CELLS_HEIGHT = 76,
   this.SPACEROCK_CELLS_HEIGHT = 145,
   this.SPACEROCK_CELLS_WIDTH = 120,
   this.SPACE_CELLS_HEIGHT = 52,

   this.PLATFORM_VELOCITY_MULTIPLIER = 4.35,
   
   // Sounds............................................................

   this.COIN_VOLUME = 1.0,
   this.SOUNDTRACK_VOLUME = 0.12,
   this.JUMP_WHISTLE_VOLUME = 0.05,
   this.PLOP_VOLUME = 0.20,
   this.THUD_VOLUME = 0.20,
   this.FALLING_WHISTLE_VOLUME = 0.10,
   this.EXPLOSION_VOLUME = 0.25,
   this.CHIMES_VOLUME = 1.0,

   // Sprite sheet cells................................................

   this.SPACE_CELLS_WIDTH = 40, // pixels
   this.SPACE_CELLS_HEIGHT = 52, // pixels

   this.SPACE_HEIGHT = 90,
   this.SPACE_JUMP_HEIGHT = 200,

   this.RUN_ANIMATION_INITIAL_RATE = 0,


   this.COIN_CELLS_HEIGHT = 28,
   this.COIN_CELLS_WIDTH  = 25,


   this.SNAIL_BOMB_CELLS_HEIGHT = 20,
   this.SNAIL_BOMB_CELLS_WIDTH  = 20,

   this.SNAIL_CELLS_HEIGHT = 34,
   this.SNAIL_CELLS_WIDTH  = 64,

   this.INITIAL_BACKGROUND_VELOCITY = 0,
   this.INITIAL_BACKGROUND_OFFSET = 0,
   this.INITIAL_SPACE_LEFT = 50,
   this.INITIAL_SPACE_TRACK = 1,
   this.INITIAL_SPACE_VELOCITY = 0,
   
   
   //Sound and music
   this.soundAndMusicElement = document.getElementById('sound-and-music'),
   this.musicElement = document.getElementById('spaceface-music'),
   this.musicElement.volume = .2,
   this.musicCheckboxElement = document.getElementById('music-checkbox'),
   this.soundCheckboxElement = document.getElementById('sound-checkbox'),
   this.musicOn = this.musicCheckboxElement.checked,
   this.audioSprites= document.getElementById('spaceface-silence'),
   this.soundOn = this.soundCheckboxElement.checked,
   
   this.audioTracks = [new Audio(), new Audio(), new Audio(), new Audio(),
                       new Audio(), new Audio(), new Audio(), new Audio()],
   
   //Initializing sounds
   this.yayySound = document.getElementById('spaceface-kermit');
   this.deadSound = document.getElementById('spaceface-ooooo');
   this.gameOverSound = document.getElementById('spaceface-silence');
   
   //Sound-related constants
   this.YAY_VOLUME = 1.0,
   this.DEATH_VOLUME = 1.0,
   this.GAME_OVER_VOLUME = 1.0,
   
   
  
//   spaceface.soundCheckbox.onchange = function(e)
//   {
//       spaceface.soundOn = spaceface.soundCheckbox.checked;
//   };
//   
//    spaceface.musicCheckbox.onchange = function(e)
//   {
//       spaceface.musicOn = spaceface.musicCheckbox.checked;
//       
//       if (spaceface.musicOn) 
//       {
//            spaceface.soundtrack.play();
//       }
//       else 
//       {
//            spaceface.soundtrack.pause();
//       }
//   };

   this.audioChannels = [
      {playing: false, audio: this.audioSprites},
      {playing: false, audio: null},
      {playing: false, audio: null},
      {playing: false, audio: null}],

   this.audioSpriteCountdown = this.audioChannels.length - 1,
   this.graphicsReady = false;
   

   // Paused............................................................
   
   this.paused = false,
   this.pauseStartTime = 0,
   this.totalTimePaused = 0,

   this.windowHasFocus = true,

   // Track baselines...................................................

   this.TRACK_1_BASELINE = 323,
   this.TRACK_2_BASELINE = 223,
   this.TRACK_3_BASELINE = 123,

   // Fps indicator.....................................................
   
   this.fpsToast = document.getElementById('fps'),

   // Images............................................................
   
   this.background  = new Image(),
   this.spritesheet = new Image(),

   // Sounds............................................................

   this.soundCheckbox = document.getElementById('sound-checkbox');
   this.musicCheckbox = document.getElementById('music-checkbox');

   this.soundOn = this.soundCheckbox.checked;
   this.musicOn = this.musicCheckbox.checked;

   this.audioTracks = [ // 8 tracks is more than enough
      new Audio(), new Audio(), new Audio(), new Audio(), 
      new Audio(), new Audio(), new Audio(), new Audio()
   ],

   this.soundtrack = document.getElementById('soundtrack'),
   this.chimesSound = document.getElementById('chimes-sound'),
   this.plopSound = document.getElementById('plop-sound'),
   this.explosionSound = document.getElementById('explosion-sound'),
   this.fallingWhistleSound = document.getElementById('whistle-down-sound'),
   this.coinSound = document.getElementById('coin-sound'),
   this.jumpWhistleSound = document.getElementById('jump-sound'),
   this.thudSound = document.getElementById('thud-sound'),

   // Time..............................................................
   
   this.lastAnimationFrameTime = 0,
   this.lastFpsUpdateTime = 0,
   this.fps = 60,

   // Translation offsets...............................................
   
   this.backgroundOffset = this.INITIAL_BACKGROUND_OFFSET,
   this.spriteOffset = this.INITIAL_BACKGROUND_OFFSET,

   // Velocities........................................................

   this.bgVelocity = this.INITIAL_BACKGROUND_VELOCITY,
   this.platformVelocity,

   // Platforms.........................................................

   this.platformData = [
      // Screen 1.......................................................
      {
         left:      10,
         width:     230,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'rgb(150,190,255)',
         opacity:   0,
         track:     4,
         pulsate:   false
      },

      {  left:      250,
         width:     300,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'rgb(150,190,255)',
         opacity:   0,
         track:     4,
         pulsate:   false
      },

      {  left:      400,
         width:     125,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'rgb(250,0,0)',
         opacity:   0,
         track:     4,
         pulsate:   true
      },

      {  left:      633,
         width:     100,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'rgb(80,140,230)',
         opacity:   0,
         track:     4,
         pulsate:   false
      },

      // Screen 2.......................................................
               
      {  left:      810,
         width:     100,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'rgb(200,200,0)',
         opacity:   0,
         track:     4,
         pulsate:   false
      },

      {  left:      1025,
         width:     100,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'rgb(80,140,230)',
         opacity:   0,
         track:     4,
         pulsate:   false
      },

      {  left:      1200,
         width:     125,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'aqua',
         opacity:   0,
         track:     4,
         pulsate:   false
      },

      {  left:      1400,
         width:     180,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'rgb(80,140,230)',
         opacity:   0,
         track:     4,
         pulsate:   false
      }

   ],


   this.spaceCellsRight = [
      { left: 29, top: 3, width: 99, height: 84 },
      { left: 148, top: 13, width: 90, height: 65 },
      { left: 273, top: 12, width: 82, height: 69 },
      { left: 370, top: 5, width: 79, height: 102 },
      { left: 473, top: 7, width: 83, height: 118 },
      { left: 570, top: 7, width: 98, height: 116 },
      { left: 674, top: 15, width: 93, height: 79 },
      { left: 785, top: 5, width: 90, height: 76 },
      { left: 885, top: 2, width: 85, height: 97 },
      { left: 986, top: 6, width: 80, height: 95 }
   ],
           
           
  // ********************** Satalite ********************** 


   this.sataliteData = [
       
      //Level One
      {left: 400, top: this.TRACK_1_BASELINE},
      {left: 400, top: this.TRACK_1_BASELINE},
      {left: 1200, top: this.TRACK_1_BASELINE},
      {left: 1200, top: this.TRACK_1_BASELINE},
      {left: 1800, top: this.TRACK_1_BASELINE},
      {left: 1800, top: this.TRACK_2_BASELINE},
      {left: 2400, top: this.TRACK_1_BASELINE},
      {left: 2400, top: this.TRACK_1_BASELINE},
      {left: 2900, top: this.TRACK_2_BASELINE},
      {left: 2900, top: this.TRACK_1_BASELINE},
      {left: 3500, top: this.TRACK_1_BASELINE},
      {left: 3500, top: this.TRACK_1_BASELINE}
   ],
           
   //  ********************** SpaceRock ********************** 
            
   this.rockData = [
       //Level One
      { left: 850,  top: this.TRACK_1_BASELINE},
      { left: 1400,  top: this.TRACK_1_BASELINE},
      { left: 2100,  top: this.TRACK_1_BASELINE},
      { left: 2600,  top: this.TRACK_1_BASELINE},
      
       //Level two
      { left: 4400,  top: this.TRACK_1_BASELINE},
      { left: 5500,  top: this.TRACK_1_BASELINE},
      { left: 6600,  top: this.TRACK_1_BASELINE},
      
       //Level Three
      { left: 8400,  top: this.TRACK_1_BASELINE},
      { left: 10100,  top: this.TRACK_1_BASELINE},
      { left: 10650,  top: this.TRACK_1_BASELINE},
      
      
       //Level Four
      { left: 13400,  top: this.TRACK_1_BASELINE},
      { left: 15100,  top: this.TRACK_1_BASELINE},
      { left: 15650,  top: this.TRACK_1_BASELINE},
      
      
       //Level Five
      { left: 19400,  top: this.TRACK_3_BASELINE},
      { left: 21050,  top: this.TRACK_2_BASELINE},
      { left: 21600,  top: this.TRACK_1_BASELINE} 
      
   ],        
   
   // Coins.............................................................

   this.coinData = [
      { left: 303,  top: this.TRACK_3_BASELINE - this.COIN_CELLS_HEIGHT }, 
      { left: 469,  top: this.TRACK_3_BASELINE - 2*this.COIN_CELLS_HEIGHT }, 
      { left: 600,  top: this.TRACK_1_BASELINE - this.COIN_CELLS_HEIGHT }, 
      { left: 833,  top: this.TRACK_2_BASELINE - 2*this.COIN_CELLS_HEIGHT }, 
      { left: 1050, top: this.TRACK_2_BASELINE - 2*this.COIN_CELLS_HEIGHT }, 
      { left: 1500, top: this.TRACK_1_BASELINE - 2*this.COIN_CELLS_HEIGHT }, 
      { left: 1670, top: this.TRACK_2_BASELINE - 2*this.COIN_CELLS_HEIGHT }, 
      { left: 1870, top: this.TRACK_1_BASELINE - 2*this.COIN_CELLS_HEIGHT }, 
      { left: 1930, top: this.TRACK_1_BASELINE - 2*this.COIN_CELLS_HEIGHT }, 
      { left: 2200, top: this.TRACK_3_BASELINE - 3*this.COIN_CELLS_HEIGHT }
   ],
           
           
           
   this.sataliteCells = [
	{ left: 20, top: 101, width: this.SATALITE_CELLS_WIDTH,height: this.SATALITE_CELLS_HEIGHT }
   ],


   // Snails............................................................

   this.snailData = [
      { platformIndex: 7 }
   ],
   
   // Spritesheet cells................................................


   this.coinCells = [
      { left: 2, top: 540, width: this.COIN_CELLS_WIDTH, height: this.COIN_CELLS_HEIGHT }
   ],
           
    this.rockCells = [
      { left: 19, top: 189, width: 84,height: 74},
      { left: 112,top: 189, width: 89,height: 80},
      { left: 206,top: 183, width: 89,height: 86},
      { left: 302,top: 186, width: 89,height: 87},
      { left: 397,top: 189, width: 88,height: 85},
      { left: 491,top: 193, width: 89,height: 75},
      { left: 585,top: 199, width: 89,height: 64},
      { left: 678,top: 201, width: 89,height: 57},
      { left: 773,top: 196, width: 89,height: 66},
      { left: 867,top: 194, width: 79,height: 73},
      { left: 957,top: 192, width: 74,height: 83},
      { left: 18,top: 308, width: 82,height: 87},
      { left: 111,top: 311, width: 81,height: 84},
      { left: 196,top: 311, width: 77,height: 89},
      { left: 277,top: 313, width: 86,height: 87},
      { left: 370,top: 313, width: 89,height: 86},
      { left: 464,top: 314, width: 89,height: 79},
      { left: 556,top: 319, width: 90,height: 68},
      { left: 651,top: 325, width: 90,height: 67}
   ],

  this.explosionCells = [
      { left: 180, top: 126, width: 50, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 244, top: 126, width: 68, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 327, top: 126, width: 68, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 408, top: 126, width: 68, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 487, top: 126, width: 68, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 567, top: 126, width: 68, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 652, top: 126, width: 68, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 734, top: 126, width: 68, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 816, top: 126, width: 68, height: this.EXPLOSION_CELLS_HEIGHT }
   ],


   this.snailBombCells = [
      { left: 2, top: 512, width: 30, height: 20 }
   ],

   this.snailCells = [
      { left: 142, top: 466, width: this.SNAIL_CELLS_WIDTH,
                             height: this.SNAIL_CELLS_HEIGHT },

      { left: 75,  top: 466, width: this.SNAIL_CELLS_WIDTH, 
                             height: this.SNAIL_CELLS_HEIGHT },

      { left: 2,   top: 466, width: this.SNAIL_CELLS_WIDTH, 
                             height: this.SNAIL_CELLS_HEIGHT }
   ],

   // Sprites...........................................................
  
   this.coins        = [],
   this.satalites    = [], 
   this.rocks        = [],
   this.platforms    = [],
   this.snails       = [],
   
   // Sprite artists...................................................

   this.spaceArtist = new SpriteSheetArtist(this.spritesheet,
                                             this.spaceCellsRight),

   this.platformArtist = {
      draw: function (sprite, context) {
         var top;
         
         context.save();

         top = spaceface.calculatePlatformTop(sprite.track);

         context.lineWidth = spaceface.PLATFORM_STROKE_WIDTH;
         context.strokeStyle = spaceface.PLATFORM_STROKE_STYLE;
         context.fillStyle = sprite.fillStyle;

         context.strokeRect(sprite.left, top, sprite.width, sprite.height);
         context.fillRect  (sprite.left, top, sprite.width, sprite.height);

         context.restore();
      }
   },

   this.runBehavior = {


      lastAdvanceTime: 0,
      
      execute: function(sprite, time, fps) {
         if (sprite.runAnimationRate === 0) {
            return;
         }
         
         if (this.lastAdvanceTime === 0) {  // skip first time
            this.lastAdvanceTime = time;
         }
         else if (time - this.lastAdvanceTime > 1000 / sprite.runAnimationRate) {
            sprite.artist.advance();
            this.lastAdvanceTime = time;
         }
      }
   },


   this.jumpBehavior = {
      pause: function (sprite) {
         if (sprite.ascendAnimationTimer.isRunning()) {
            sprite.ascendAnimationTimer.pause();
         }
         else if (!sprite.descendAnimationTimer.isRunning()) {
            sprite.descendAnimationTimer.pause();
         }
      },

      unpause: function (sprite) {
         if (sprite.ascendAnimationTimer.isRunning()) {
            sprite.ascendAnimationTimer.unpause();
         }
         else if (sprite.descendAnimationTimer.isRunning()) {
            sprite.descendAnimationTimer.unpause();
         }
      },

      jumpIsOver: function (sprite) {
         return ! sprite.ascendAnimationTimer.isRunning() &&
                ! sprite.descendAnimationTimer.isRunning();
      },

      // Ascent...............................................................

      isAscending: function (sprite) {
         return sprite.ascendAnimationTimer.isRunning();
      },
      
      ascend: function (sprite) {
         var elapsed = sprite.ascendAnimationTimer.getElapsedTime(),
             deltaH  = elapsed / (sprite.JUMP_DURATION/2) * sprite.JUMP_HEIGHT;
         sprite.top = sprite.verticalLaunchPosition - deltaH;
      },

      isDoneAscending: function (sprite) {
         return sprite.ascendAnimationTimer.getElapsedTime() > sprite.JUMP_DURATION/2;
      },
      
      finishAscent: function (sprite) {
         sprite.jumpApex = sprite.top;
         sprite.ascendAnimationTimer.stop();
         sprite.descendAnimationTimer.start();
      },
      
      // Descents.............................................................

      isDescending: function (sprite) {
         return sprite.descendAnimationTimer.isRunning();
      },

      descend: function (sprite, verticalVelocity, fps) {
         var elapsed = sprite.descendAnimationTimer.getElapsedTime(),
             deltaH  = elapsed / (sprite.JUMP_DURATION/2) * sprite.JUMP_HEIGHT;

         sprite.top = sprite.jumpApex + deltaH;
      },
      
      isDoneDescending: function (sprite) {
         return sprite.descendAnimationTimer.getElapsedTime() > sprite.JUMP_DURATION/2;
      },

      finishDescent: function (sprite) {
         sprite.stopJumping();

         if (spaceface.isOverPlatform(sprite) !== -1) {
            sprite.top = sprite.verticalLaunchPosition;
         }
         else {
            sprite.fall(spaceface.GRAVITY_FORCE *
                        (sprite.descendAnimationTimer.getElapsedTime()/1000) *
                        spaceface.PIXELS_PER_METER);
         }
      },
      
      // Execute..............................................................

      execute: function(sprite, time, fps) {
         if ( ! sprite.jumping || sprite.exploding) {
            return;
         }

         if (this.jumpIsOver(sprite)) {
            sprite.jumping = false;
            return;
         }

         if (this.isAscending(sprite)) {
            if ( ! this.isDoneAscending(sprite)) { this.ascend(sprite); }
            else                                 { this.finishAscent(sprite); }
         }
         else if (this.isDescending(sprite)) {
            if ( ! this.isDoneDescending(sprite)) { this.descend(sprite); }
            else                                  { this.finishDescent(sprite); }
         }
      } 
   },


   this.fallBehavior = {
      isOutOfPlay: function (sprite) {
         return sprite.top > spaceface.TRACK_1_BASELINE;
      },

      willFallBelowCurrentTrack: function (sprite, deltaY) {
         return sprite.top + sprite.height + deltaY >
                spaceface.calculatePlatformTop(sprite.track);
      },

      fallOnPlatform: function (sprite) {
         sprite.top = spaceface.calculatePlatformTop(sprite.track) - sprite.height;
         sprite.stopFalling();
      },

      setSpriteVelocity: function (sprite) {
         var fallingElapsedTime;

         sprite.velocityY = sprite.initialVelocityY + spaceface.GRAVITY_FORCE *
                            (sprite.fallAnimationTimer.getElapsedTime()/1000) *
                            spaceface.PIXELS_PER_METER;
      },

      calculateVerticalDrop: function (sprite, fps) {
         return sprite.velocityY / fps;
      },

      isPlatformUnderneath: function (sprite) {
         return spaceface.isOverPlatform(sprite) !== -1;
      },
      
      execute: function (sprite, time, fps) {
         var deltaY;

         if (sprite.jumping) {
            return;
         }

         if (this.isOutOfPlay(sprite) || sprite.exploding) {
            if (sprite.falling) {
               sprite.stopFalling();
            }
            return;
         }
         
         if (!sprite.falling) {
            if (!sprite.exploding && !this.isPlatformUnderneath(sprite)) {
               sprite.fall();
            }
            return;
         }

         this.setSpriteVelocity(sprite);
         deltaY = this.calculateVerticalDrop(sprite, fps);
               
         if (!this.willFallBelowCurrentTrack(sprite, deltaY)) {
            sprite.top += deltaY;
         }
         else { // will fall below current track

            if (this.isPlatformUnderneath(sprite)) {
               this.fallOnPlatform(sprite);
               sprite.stopFalling();
            }
            else {
               sprite.track--;

               sprite.top += deltaY;

               }
            }
         }
   },

   this.collideBehavior = {
      execute: function (sprite, time, fps, context) {
         var otherSprite;

         for (var i=0; i < spaceface.sprites.length; ++i) { 
            otherSprite = spaceface.sprites[i];

            if (this.isCandidateForCollision(sprite, otherSprite)) {
               if (this.didCollide(sprite, otherSprite, context)) { 
                  this.processCollision(sprite, otherSprite);
               }
            }
         }
      },

      isCandidateForCollision: function (sprite, otherSprite) {
         return sprite !== otherSprite &&
                sprite.visible && otherSprite.visible &&
                !sprite.exploding && !otherSprite.exploding &&
                otherSprite.left - otherSprite.offset < sprite.left + sprite.width;
      }, 

      didSnailBombCollideWithSpace: function (left, top, right, bottom,
                                         snailBomb, context) {
         context.beginPath();
         context.rect(left, top, right - left, bottom - top);

         return context.isPointInPath(
                       snailBomb.left - snailBomb.offset + snailBomb.width/2,
                       snailBomb.top + snailBomb.height/2);
      },

      didSpaceCollideWithOtherSprite: function (left, top, right, bottom,
                                                 centerX, centerY,
                                                 otherSprite, context) {


         context.beginPath();
         context.rect(otherSprite.left - otherSprite.offset, otherSprite.top,
                      otherSprite.width, otherSprite.height);
         
         return context.isPointInPath(left,    top)     ||
                context.isPointInPath(right,   top)     ||

                context.isPointInPath(centerX, centerY) ||

                context.isPointInPath(left,    bottom)  ||
                context.isPointInPath(right,   bottom);
      },
     
      didCollide: function (sprite, otherSprite, context) {
         var MARGIN_TOP = 10,
             MARGIN_LEFT = 10,
             MARGIN_RIGHT = 10,
             MARGIN_BOTTOM = 0,
             left = sprite.left + sprite.offset + MARGIN_LEFT,
             right = sprite.left + sprite.offset + sprite.width - MARGIN_RIGHT,
             top = sprite.top + MARGIN_TOP,
             bottom = sprite.top + sprite.height - MARGIN_BOTTOM,
             centerX = left + sprite.width/2,
             centerY = sprite.top + sprite.height/2;

         if (otherSprite.type === 'snail bomb') {
            return this.didSnailBombCollideWithSpace(left, top, right, bottom,
                                                otherSprite, context);
         }
         else {
            return this.didSpaceCollideWithOtherSprite(left, top, right, bottom,
                                                  centerX, centerY,
                                                  otherSprite, context);
         }
      },

      processCollision: function (sprite, otherSprite) {
         if (otherSprite.value) { 
            // Keep score...
         }


         if ('coin'  === otherSprite.type    ||
             'snail bomb' === otherSprite.type) {
            otherSprite.visible = false;

         }

         if ('rock' === otherSprite.type || 
             'satalite' === otherSprite.type) {
            spaceface.explode(sprite);
         }

         if (sprite.jumping && 'platform' === otherSprite.type) {
            this.processPlatformCollisionDuringJump(sprite, otherSprite);
         }
      },

      processPlatformCollisionDuringJump: function (sprite, platform) {
         var isDescending = sprite.descendAnimationTimer.isRunning();

         sprite.stopJumping();

         if (isDescending) {
            sprite.track = platform.track; 
            sprite.top = spaceface.calculatePlatformTop(sprite.track) - sprite.height;
         }
      }
   };


   this.paceBehavior = {
      execute: function (sprite, time, fps) {
         var sRight = sprite.left + sprite.width,
             pRight = sprite.platform.left + sprite.platform.width,
             pixelsToMove = sprite.velocityX / fps;

         if (sprite.direction === undefined) {
            sprite.direction = spaceface.RIGHT;
         }

         if (sprite.velocityX === 0) {
            if (sprite.type === 'snail') {
               sprite.velocityX = spaceface.SNAIL_PACE_VELOCITY;
            }
            
         }

         if (sRight > pRight && sprite.direction === spaceface.RIGHT) {
            sprite.direction = spaceface.LEFT;
         }
         else if (sprite.left < sprite.platform.left &&
                  sprite.direction === spaceface.LEFT) {
            sprite.direction = spaceface.RIGHT;
         }

         if (sprite.direction === spaceface.RIGHT) {
            sprite.left += pixelsToMove;
         }
         else {
            sprite.left -= pixelsToMove;
         }
      }
   };



   this.snailShootBehavior = { // sprite is the snail
      execute: function (sprite, time, fps) {
         var bomb = sprite.bomb;

         if (!spaceface.spriteInView(sprite)) {
            return;
         }

         if (! bomb.visible && sprite.artist.cellIndex === 2) {
            bomb.left = sprite.left;
            bomb.visible = true;
         }
      }
   };

   this.snailBombMoveBehavior = {
      execute: function(sprite, time, fps) {  // sprite is the bomb
         if (sprite.visible && spaceface.spriteInView(sprite)) {
            sprite.left -= spaceface.SNAIL_BOMB_VELOCITY / fps;
         }

         if (!spaceface.spriteInView(sprite)) {
            sprite.visible = false;
         }
      }
   };


   this.space = new Sprite('space',           // type
                            this.spaceArtist,  // artist
                            [ this.runBehavior, // behaviors
                              this.jumpBehavior,
                              this.fallBehavior,
                              this.collideBehavior
                            ]); 

   this.space.width = this.SPACE_CELLS_WIDTH;
   this.space.height = this.SPACE_CELLS_HEIGHT;

   this.sprites = [ this.space ];  

   this.explosionAnimator = new SpriteAnimator(
      this.explosionCells,          // Animation cells
      this.EXPLOSION_DURATION,      // Duration of the explosion

      function (sprite, animator) { // Callback after animation
         sprite.exploding = false; 

         if (sprite.jumping) {
            sprite.stopJumping();
         }
         else if (sprite.falling) {
            sprite.stopFalling();
         }

         sprite.visible = true;
         sprite.track = 1;
         sprite.top = spaceface.calculatePlatformTop(sprite.track) - sprite.height;
         sprite.artist.cellIndex = 0;
         sprite.runAnimationRate = spaceface.RUN_ANIMATION_RATE;
      }
   );
};



Spaceface.prototype = {

   draw: function (now) {
      this.setPlatformVelocity();
      this.setTranslationOffsets();

      this.drawBackground();

      this.updateSprites(now);
      this.drawSprites();
      
//      if(spaceface.mobileInstructionsVisible)
//      {
//         spaceface.drawMobileInstructions();
//      }
      
   },

   setPlatformVelocity: function () {
      this.platformVelocity = this.bgVelocity * this.PLATFORM_VELOCITY_MULTIPLIER; 
   },

   setTranslationOffsets: function () {
      this.setBackgroundTranslationOffset();
      this.setSpriteTranslationOffsets();
   },
   
   setSpriteTranslationOffsets: function () {
      var i, sprite;
   
      this.spriteOffset += this.platformVelocity / this.fps; // In step with platforms

      for (i=0; i < this.sprites.length; ++i) {
         sprite = this.sprites[i];
      
         if ('space' !== sprite.type) {
            sprite.offset = this.spriteOffset; 
         }
      }
   },

   setBackgroundTranslationOffset: function () {
      var offset = this.backgroundOffset + this.bgVelocity/this.fps;
   
      if (offset > 0 && offset < this.background.width) {
         this.backgroundOffset = offset;
      }
      else {
         this.backgroundOffset = 0;
      }
   },
   
   drawBackground: function () {
      this.context.save();
   
      this.context.globalAlpha = 1.0;
      this.context.translate(-this.backgroundOffset, 0);
   

      this.context.drawImage(this.background, 0, 0,
                        this.background.width, this.background.height);
   

      this.context.drawImage(this.background, this.background.width, 0,
                        this.background.width+1, this.background.height);
   
      this.context.restore();
   },
   
  calculateFps: function (now) {
      var fps = 1000 / (now - this.lastAnimationFrameTime);
      var score = 0;
      this.lastAnimationFrameTime = now;
   
      if (now - this.lastFpsUpdateTime > 1000) {
         this.lastFpsUpdateTime = now;
         this.fpsElement.innerHTML = fps.toFixed(0) + ' fps';
         score = now / 1000;
         
          if(this.score % 5 === 0)
         {
             this.points++;
         }
         
         this.scoreElement.innerHTML = score.toFixed(0);
         this.levelElement.innerHTML = 'Level: ' + this.level;
         this.lifeElement.innerHTML = 'Lives: ' + this.lives;
         this.pointElement.innerHTML = 'Points: ' + this.points;
         
        
         
         if(score >= this.speed)
         {
             this.bgVelocity = this.bgVelocity + 10;
             this.speed = this.speed + 20;
             this.level = this.level + 1;
         }
         
         if(score%10===0)
         {
             this.playSound(this.yayySound);
         }
      }

      return fps; 
   },
   
   calculatePlatformTop: function (track) {
      var top;
   
      if      (track === 1) { top = this.TRACK_1_BASELINE; }
      else if (track === 2) { top = this.TRACK_2_BASELINE; }
      else if (track === 3) { top = this.TRACK_3_BASELINE; }

      return top;
   },

   turnLeft: function () {
      this.bgVelocity = -this.BACKGROUND_VELOCITY;
      this.space.runAnimationRate = this.RUN_ANIMATION_RATE;
      this.spaceArtist.cells = this.spaceCellsLeft;
      this.space.direction = this.LEFT;
   },

   turnRight: function () {
      this.bgVelocity = this.BACKGROUND_VELOCITY;
      this.space.runAnimationRate = this.RUN_ANIMATION_RATE;
      this.spaceArtist.cells = this.spaceCellsRight;
      this.space.direction = this.RIGHT;
   },
   

   equipSpace: function () {

      
      this.space.runAnimationRate = this.RUN_ANIMATION_INITIAL_RATE,
   
      this.space.track = this.INITIAL_SPACE_TRACK;
      this.space.direction = this.LEFT;
      this.space.velocityX = this.INITIAL_SPACE_VELOCITY;
      this.space.left = this.INITIAL_SPACE_LEFT;
      this.space.top = this.calculatePlatformTop(this.space.track) -
                        this.SPACE_CELLS_HEIGHT;

      this.space.artist.cells = this.spaceCellsRight;
      this.space.offset = 0;

      this.equipSpaceForJumping();
      this.equipSpaceForFalling();
   },

   equipSpaceForFalling: function () {
      this.space.falling = false;
      this.space.fallAnimationTimer = new AnimationTimer();

      this.space.fall = function (initialVelocity) {
         this.velocityY = initialVelocity || 0;
         this.initialVelocityY = initialVelocity || 0;
         this.fallAnimationTimer.start();
         this.falling = true;
      },

      this.space.stopFalling = function () {
         this.falling = false;
         this.velocityY = 0;
         this.fallAnimationTimer.stop();
      };
   },
   
   equipSpaceForJumping: function () {
      this.space.JUMP_DURATION = this.SPACE_JUMP_DURATION; // milliseconds
      this.space.JUMP_HEIGHT = this.SPACE_JUMP_HEIGHT;

      this.space.jumping = false;

      this.space.ascendAnimationTimer =
         new AnimationTimer(this.space.JUMP_DURATION/2,
                            AnimationTimer.makeEaseOutTransducer(1.1));

      this.space.descendAnimationTimer =
         new AnimationTimer(this.space.JUMP_DURATION/2,
                            AnimationTimer.makeEaseInTransducer(1.1));

      this.space.stopJumping = function () {
         this.jumping = false;
         this.ascendAnimationTimer.stop();
         this.descendAnimationTimer.stop();
         this.runAnimationRate = spaceface.RUN_ANIMATION_RATE;
      };
      
      this.space.jump = function () {
         if (this.jumping) 
            return;

         this.runAnimationRate = 0;
         this.jumping = true;
         this.verticalLaunchPosition = this.top;
         this.ascendAnimationTimer.start();

      };
   },
   
   createPlatformSprites: function () {
      var sprite, pd;  // Sprite, Platform data
   
      for (var i=0; i < this.platformData.length; ++i) {
         pd = this.platformData[i];
         sprite  = new Sprite('platform', this.platformArtist);

         sprite.left      = pd.left;
         sprite.width     = pd.width;
         sprite.height    = pd.height;
         sprite.fillStyle = pd.fillStyle;
         sprite.opacity   = pd.opacity;
         sprite.track     = pd.track;
         sprite.pulsate   = pd.pulsate;

         sprite.top = this.calculatePlatformTop(pd.track);
   
         if (sprite.pulsate) {
            sprite.behaviors = [ new PulseBehavior(1000, 0.5) ];
         }

         this.platforms.push(sprite);
      }
   },
 
    explode: function (sprite, silent) {
      if (sprite.spaceAnimationRate === 0) {
         sprite.spaceAnimationRate = this.SPACE_ANIMATION_RATE;
      }
           
      sprite.exploding = true;
      
      this.lives = this.lives - 1;
           this.playSound(this.deadSound);
      this.explosionAnimator.start(sprite, true); 
      
      if(this.lives === 0)
         {
              
             spaceface.start();
         }
     
   },

   // Animation............................................................

   animate: function (now) { 
      if (spaceface.paused) {
         setTimeout( function () {
            requestNextAnimationFrame(spaceface.animate);
         }, spaceface.PAUSED_CHECK_INTERVAL);
      }
      else {
         spaceface.fps = spaceface.calculateFps(now); 
         spaceface.draw(now);
         requestNextAnimationFrame(spaceface.animate);
      }
   },

   togglePausedStateOfAllBehaviors: function () {
      var behavior;
   
      for (var i=0; i < this.sprites.length; ++i) { 
         sprite = this.sprites[i];

         for (var j=0; j < sprite.behaviors.length; ++j) { 
            behavior = sprite.behaviors[j];

            if (this.paused) {
               if (behavior.pause) {
                  behavior.pause(sprite);
               }
            }
            else {
               if (behavior.unpause) {
                  behavior.unpause(sprite);
               }
            }
         }
      }
   },
   
         soundLoaded: function()
      {
         spaceface.audioSpriteCountdown--;
         if(spaceface.audioSpriteCountdown === 0)
         {
            if(!spaceface.gameStarted && spaceface.graphicsReady)
            {
               spaceface.startGame();
            }
         }
      },
      
      soundIsPlaying: function (sound)
      {
          return !sound.ended && sound.currentTime >0;
      },

      playSound: function(sound)
      {
         var track, index;

      if (this.soundOn) {
         if (!this.soundIsPlaying(sound)) {
            sound.play();
         }
         else {
            for (i=0; index < this.audioTracks.length; ++index) {
               track = this.audioTracks[index];
            
               if (!this.soundIsPlaying(track)) {
                  track.src = sound.currentSrc;
                  track.load();
                  track.volume = sound.volume;
                  track.play();

                  break;
               }
            }
         }              
      }
   },

   togglePaused: function () {
      var now = +new Date();

      this.paused = !this.paused;
      this.togglePausedStateOfAllBehaviors();
   
      if (this.paused) {
         this.pauseStartTime = now;
      }
      else {
         this.lastAnimationFrameTime += (now - this.pauseStartTime);
      }

      if (this.paused && this.musicOn) {
         this.soundtrack.pause();
      }
      else if (!this.paused && this.musicOn) {
         this.soundtrack.play();
      }
   },



   initializeSounds: function () {
      //this.soundtrack.volume          = this.SOUNDTRACK_VOLUME;
      //.jumpWhistleSound.volume    = this.JUMP_WHISTLE_VOLUME;
      //this.thudSound.volume           = this.THUD_VOLUME;
      //this.fallingWhistleSound.volume = this.FALLING_WHISTLE_VOLUME;
      //this.chimesSound.volume         = this.CHIMES_VOLUME;
      //this.explosionSound.volume      = this.EXPLOSION_VOLUME;
      //this.coinSound.volume           = this.COIN_VOLUME;
   },


   // ------------------------- INITIALIZATION ----------------------------

   start: function () {
      this.createSprites();
      this.initializeImages();
      this.initializeSounds();
      this.equipSpace();
      this.splashToast('Good Luck!');

      document.getElementById('instructions').style.opacity =
         spaceface.INSTRUCTIONS_OPACITY;
   },
   
   initializeImages: function () {
      var self = this;

      this.background.src = 'images/canvasBackground.jpg';
      this.spritesheet.src = 'images/spritesheet.png';
   
      this.background.onload = function (e) {
         self.startGame();
      };
   },

   startGame: function () {
       this.lives = 5,
       this.points = 0;
       this.level = 0;
       
//        if(spaceface.mobile)
//      {
//         this.fadeInElements(spaceface.mobileWelcomeToast);
//      }
//      else
//      {   
//		    this.revealInitialToast();
//      }

       this.startMusic();
      requestNextAnimationFrame(this.animate);
   },
   
   startMusic: function()
      {
         var MUSIC_DELAY = 1000;

         setTimeout(function()
         {
            if(spaceface.musicCheckboxElement.checked)
            {
               spaceface.musicElement.play();
            }
            spaceface.pollMusic();
         }, MUSIC_DELAY);
      },
      
      pollMusic: function()
      {
         var POLL_INTERVAL = 500,
         SOUNDTRACK_LENGTH = 132,
         timerID;

         timerID = setInterval(function()
         {
            if(spaceface.musicElement.currentTime > SOUNDTRACK_LENGTH)
            {
               clearInterval(timerID); //stop polling
               spaceface.restartMusic();
            }
         }, POLL_INTERVAL);
      },

      restartMusic: function()
      {
         spaceface.musicElement.pause();
         spaceface.musicElement.currentTime = 0;
         spaceface.startMusic();
      },
      
            createAudioChannels: function()
      {
         var channel;

         for(var i=0; i < this.audioChannels.length; ++i)
         {
            channel = this.audioChannels[i];
            if(i !== 0)
            {
               channel.audio = document.createElement('audio');
               channel.audio.addEventListener('loaddata',
                  this.soundLoaded, false);
               channel.audio.src = this.audioSprites.currentSrc;
            }
            channel.audio.autobuffer = true;
         }
      },


      getFirstAvailableAudioChannel: function()
      {
         for(var i =0; i < this.audioChannels.length; ++i)
         {
            if(!this.audioChannels[i].playing)
            {
               console.log(i);
               return this.audioChannels[i];
            }
         }
         return null;
      },

      seekAudio: function(sound, audio)
      {
         try
         {
            audio.pause();
            audio.currentTime = sound.position;
         }
         catch(e)
         {
            console.error('Cannot play audio');
         }
      },

      playAudio: function(audio, channel)
      {
         try
         {
            audio.play();
            channel.playing = true;
         }
         catch(e)
         {
            if(console)
            {
               console.error('Cannot play audio');
            }
         }
      },

   positionSprites: function (sprites, spriteData) {
      var sprite;

      for (var i = 0; i < sprites.length; ++i) {
         sprite = sprites[i];

         if (spriteData[i].platformIndex) { 
            this.putSpriteOnPlatform(sprite,
               this.platforms[spriteData[i].platformIndex]);
         }
         else {
            sprite.top  = spriteData[i].top;
            sprite.left = spriteData[i].left;
         }
      }
   },

   armSnails: function () {
      var snail,
          snailBombArtist = new SpriteSheetArtist(this.spritesheet, this.snailBombCells);

      for (var i=0; i < this.snails.length; ++i) {
         snail = this.snails[i];
         snail.bomb = new Sprite('snail bomb',
                                  snailBombArtist,
                                  [ this.snailBombMoveBehavior ]);

         snail.bomb.width  = spaceface.SNAIL_BOMB_CELLS_WIDTH;
         snail.bomb.height = spaceface.SNAIL_BOMB_CELLS_HEIGHT;

         snail.bomb.top = snail.top + snail.bomb.height/2;
         snail.bomb.left = snail.left + snail.bomb.width/2;
         snail.bomb.visible = false;

         snail.bomb.snail = snail;  // Snail bombs maintain a reference to their snail

         this.sprites.push(snail.bomb);
      }
   },
   
   addSpritesToSpriteArray: function () {
      for (var i=0; i < this.platforms.length; ++i) {
         this.sprites.push(this.platforms[i]);
      }

      for (var i=0; i < this.coins.length; ++i) {
         this.sprites.push(this.coins[i]);
      }
      
       for (var i=0; i < this.satalites.length; ++i) {
         this.sprites.push(this.satalites[i]);
      }
      
      for (var i=0; i < this.rocks.length; ++i) {
         this.sprites.push(this.rocks[i]);
      }

     for (var i=0; i < this.snails.length; ++i) {
         this.sprites.push(this.snails[i]);
      }
   },


   
   createCoinSprites: function () {
      var coin,
          coinArtist = new SpriteSheetArtist(this.spritesheet, this.coinCells);
   
      for (var i = 0; i < this.coinData.length; ++i) {
         coin = new Sprite('coin', coinArtist);

         coin.width = this.COIN_CELLS_WIDTH;
         coin.height = this.COIN_CELLS_HEIGHT;

         this.coins.push(coin);
      }
   },
   

   
   createSnailSprites: function () {
      var snail,
          snailArtist = new SpriteSheetArtist(this.spritesheet, this.snailCells);
   
      for (var i = 0; i < this.snailData.length; ++i) {
         snail = new Sprite('snail',
                            snailArtist,
                            [ this.paceBehavior,
                              this.snailShootBehavior,
                              new CycleBehavior(300, 1500)
                            ]);

         snail.width  = this.SNAIL_CELLS_WIDTH;
         snail.height = this.SNAIL_CELLS_HEIGHT;

         this.snails.push(snail);
      }
   },
   
   createRocksSprites: function () {
      var rock,
          rockArtist = new SpriteSheetArtist(this.spritesheet, this.rockCells);
   
      for (var i = 0; i < this.rockData.length; ++i) {
         rock = new Sprite('rock', rockArtist,
                               [ new CycleBehavior(this.ROCK_SPARKLE_DURATION,
                                           this.ROCK_SPARKLE_INTERVAL),

                                 new BounceBehavior()
                               ]);

         rock.width = this.SPACEROCK_CELLS_WIDTH;
         rock.height = this.SPACEROCK_CELLS_HEIGHT;

         this.rocks.push(rock);
      }
   },
   
   createSatalitesSprites: function () {
      var sat,
          satArtist = new SpriteSheetArtist(this.spritesheet, this.sataliteCells);

      for (var i = 0; i < this.sataliteData.length; ++i) {
         sat = new Sprite('satalite', satArtist);

         sat.width = this.SATALITE_CELLS_WIDTH;
         sat.height = this.SATALITE_CELLS_HEIGHT;

         this.satalites.push(sat);
      }
   },
   
   updateSprites: function (now) {
      var sprite;
   
      for (var i=0; i < this.sprites.length; ++i) {
         sprite = this.sprites[i];

         if (sprite.visible && this.spriteInView(sprite)) {
            sprite.update(now, this.fps, this.context);
         }
      }
   },
   
   drawSprites: function() {
      var sprite;
   
      for (var i=0; i < this.sprites.length; ++i) {
         sprite = this.sprites[i];

         if (sprite.visible && this.spriteInView(sprite)) {
            this.context.translate(-sprite.offset, 0);

            sprite.draw(this.context);

            this.context.translate(sprite.offset, 0);
         }
      }
   },
   
   spriteInView: function(sprite) {
      return sprite === this.space || // runner is always visible
         (sprite.left + sprite.width > this.spriteOffset &&
          sprite.left < this.spriteOffset + this.canvas.width);   
   },

   isOverPlatform: function (sprite, track) {
      var p,
          index = -1,
          center = sprite.left + sprite.offset + sprite.width/2;

      if (track === undefined) { 
         track = sprite.track; // Look on sprite track only
      }

      for (var i=0; i < spaceface.platforms.length; ++i) {
         p = spaceface.platforms[i];

         if (track === p.track) {
            if (center > p.left - p.offset && center < (p.left - p.offset + p.width)) {
               index = i;
               break;
            }
         }
      }
      return index;
   },
   
   putSpriteOnPlatform: function(sprite, platformSprite) {
      sprite.top  = platformSprite.top - sprite.height;
      sprite.left = platformSprite.left;
      sprite.platform = platformSprite;
   },
   
   createSprites: function() {  
      this.createPlatformSprites(); // Platforms must be created first
      
      this.createCoinSprites();
      this.createSnailSprites();
      this.createRocksSprites();
      this.initializeSprites();
      this.createSatalitesSprites();
      this.addSpritesToSpriteArray();
   },
   
   initializeSprites: function() {  
      for (var i=0; i < spaceface.sprites.length; ++i) { 
         spaceface.sprites[i].offset = 0;
      }
      this.positionSprites(this.coins,      this.coinData);
      this.positionSprites(this.rocks,      this.rockData);
      this.positionSprites(this.satalites, this.sataliteData);
      this.positionSprites(this.snails,     this.snailData);

      this.armSnails();
   },

   // Toast................................................................

   splashToast: function (text, howLong) {
      howLong = howLong || this.DEFAULT_TOAST_TIME;

      toast.style.display = 'block';
      toast.innerHTML = text;

      setTimeout( function (e) {
         if (spaceface.windowHasFocus) {
            toast.style.opacity = 1.0; // After toast is displayed
         }
      }, 50);

      setTimeout( function (e) {
         if (spaceface.windowHasFocus) {
            toast.style.opacity = 0; // Starts CSS3 transition
         }

         setTimeout( function (e) { 
            if (spaceface.windowHasFocus) {
               toast.style.display = 'none'; 
            }
         }, 480);
      }, howLong);
   }
   
//   detectMobile: function()
//   {
//      spaceface.mobile = 'ontouchstart' in window;
//      console.log(spaceface.mobile);
//   },
//
//   fitScreen: function()
//   {
//      var arenaSize = spaceface.calculateArenaSize(spaceface.getViewportSize());
//      spaceface.resizeElementsToFitScreen(arenaSize.width, arenaSize.height);
//   },
//
//   getViewportSize: function()
//   {
//      return { 
//         width: Math.max(document.documentElement.clientWidth 
//         || window.innerWidth || 0), 
//         height: Math.max(document.documentElement.clientHeight 
//         || window.innerHeight || 0)};
//   },
//   
//   drawMobileInstructions: function()
//   {
//      var cw = this.canvas.width,
//          ch = this.canvas.height,
//          TOP_LINE_OFFSET = 115,
//          LINE_HEIGHT = 40;
//
//      this.context.save();
//      this.initializeContextForMobileInstruction();
//      this.drawMobileDivider(cw, ch);
//      this.drawMobileInstructionsLeft(cw, ch, TOP_LINE_OFFSET, LINE_HEIGHT);
//      this.drawMobileInstructionsRight(cw, ch, TOP_LINE_OFFSET, LINE_HEIGHT);
//      this.context.restore();
//   },
//
//   initializeContextForMobileInstruction: function()
//   {
//      this.context.textAlign = 'center';
//      this.context.textBaseLine = 'middle';
//      this.context.font = '26px fantasy';
//      this.context.shadowBlur = 2;
//      this.context.shadowOffsetX = 2;
//      this.context.shadowOffsetY = 2;
//      this.context.shadowColor = 'black';
//      this.context.fillStyle = 'yellow';
//      this.context.strokeStyle = 'yellow';
//   },
//
//   drawMobileDivider: function(cw, ch)
//   {
//      this.context.beginPath();
//      this.context.moveTo(cw/2, 0);
//      this.context.lineTo(cw/2, ch);
//      this.context.stoke();
//   },
//
//   drawMobileInstructionsLeft: function(cw, ch, topLineOffset, lineHeight)
//   {
//      this.context.font = '32px fantasy';
//      this.context.fillText('Tap on this side to:', cw/4, ch/2-topLineOffset);
//      this.context.fillStyle = 'white';
//      this.context.font = 'italic 26px fantasy';
//      this.context.fillText('Turn around when running right', cw/4,
//         ch/2 - topLineOffset + 2 * lineHeight);
//      this.context.fillText('Jump when running left', cw/4,
//         ch/2 - topLineOffset + 3 * lineHeight);
//   },
//
//   drawMobileInstructionsRight: function(cw, ch, topLineOffset, lineHeight)
//   {
//      this.context.font = '32px fantasy';
//      this.context.fillText('Tap on this side to:', 3*cw/4, ch/2-topLineOffset);
//      this.context.fillStyle = 'white';
//      this.context.font = 'italic 26px fantasy';
//      this.context.fillText('Turn around when running left', 3*cw/4,
//         ch/2 - topLineOffset + 2 * lineHeight);
//      this.context.fillText('Jump when running right', 3*cw/4,
//         ch/2 - topLineOffset + 3 * lineHeight);
//      this.context.fillText('Start running right', 3*cw/4,
//         ch/2 - topLineOffset + 5 * lineHeight);
//   },
//   
//    addTouchEventHandlers: function()
//   {
//      spaceface.canvas.addEventListener('touchstart', spaceface.touchStart);
//      spaceface.canvas.addEventListener('touchend', spaceface.touchEnd);
//   },
//
//   touchStart: function(e)
//   {
//      if(spaceface.playing)
//      {
//         //Prevent players from inadvertently dragging the game canvas
//         e.preventDefault();
//      }
//   },
//
//   touchEnd: function(e)
//   {
//      var x = e.changedTouches[0].pageX;
//      if(spaceface.playing)
//      {
//         if(x < spaceface.canvas.width/2)
//         {
//            spaceface.processLeftTap();
//         }
//         else if(x > spaceface.canvas.width/2)
//         {
//            spaceface.processRightTap();
//         }
//         //Prevent players from double tapping to zoom 
//         //when the game is playing
//         e.preventDefault();
//      }
//   },
//
//   processLeftTap: function()
//   {
//      if(spaceface.runner.direction === spaceface.RIGHT)
//      {
//         spaceface.turnLeft();
//      }
//      else
//      {
//         spaceface.runner.jump();
//      }
//   },
//
//   processRightTap: function()
//   {
//      if(spaceface.runner.direction === spaceface.LEFT)
//      {
//         spaceface.turnRight();
//      }
//      else
//      {
//         spaceface.runner.jump();
//      }
//   }
};
   
// Event processrs.......................................................

//spaceface.welcomeStartLink.addEventListener('click',
//   function(e)
//   {
//      var FADE_DURATION = 1000;
//      spaceface.playSound(spaceface.coinSound);
//      spaceface.fadeOutElements(spaceface.mobileWelcomeToast, FADE_DURATION);
//      spaceface.playing = true;
//   });

//spaceface.mobileStartLink.addEventListener('click',
//   function(e)
//   {
//      var FADE_DURATION = 1000;
//      spaceface.fadeOutElements(spaceface.mobileStartToast, FADE_DURATION);
//      spaceface.mobileInstructionsVisible = false;
//      spaceface.playSound(spaceface.coinSound);
//      spaceface.playing = true;
//   });
//
//snailBait.showHowLink.addEventListener('click',
//   function(e)
//   {
//      var FADE_DURATION = 1000;
//      spaceface.fadeOutElements(spaceface.mobileWelcomeToast, FADE_DURATION);
//      spaceface.drawMobileInstructions();
//      spaceface.revealMobileStartToast();
//      spaceface.mobileInstructionsVisible = true;
//   });


window.onkeydown = function (e) {
   var key = e.keyCode;

   if (key === 80 || (spaceface.paused && key !== 80)) {  // 'p'
      spaceface.togglePaused();
   }
   if (key === 75 || key === 39) { // 'k' or right arrow
      spaceface.turnRight();
   }
   else if (key === 74) { // 'j'
      if (!spaceface.space.jumping && !spaceface.space.falling) {
         spaceface.space.jump();
      }
   }
};

window.onblur = function (e) {  // pause if unpaused
   spaceface.windowHasFocus = false;
   
   if (!spaceface.paused) {
      spaceface.togglePaused();
   }
};

window.onfocus = function (e) {  // unpause if paused
   var originalFont = spaceface.toast.style.fontSize;

   spaceface.windowHasFocus = true;

   if (spaceface.paused) {
      spaceface.toast.style.font = '128px fantasy';

      spaceface.splashToast('3', 500); // Display 3 for one half second

      setTimeout(function (e) {
         spaceface.splashToast('2', 500); // Display 2 for one half second

         setTimeout(function (e) {
            spaceface.splashToast('1', 500); // Display 1 for one half second

            setTimeout(function (e) {
               if ( spaceface.windowHasFocus) {
                  spaceface.togglePaused();
               }

               setTimeout(function (e) { // Wait for '1' to disappear
                  spaceface.toast.style.fontSize = originalFont;
               }, 2000);
            }, 1000);
         }, 1000);
      }, 1000);
   }
};

// Launch game.........................................................

var spaceface = new Spaceface();
spaceface.start();

// Sound and music controls............................................

spaceface.soundCheckbox.onchange = function (e) {
   spaceface.soundOn = spaceface.soundCheckbox.checked;
};

spaceface.musicCheckbox.onchange = function (e) {
   spaceface.musicOn = spaceface.musicCheckbox.checked;

   if (spaceface.musicOn) {
      spaceface.soundtrack.play();
   }
   else {
      spaceface.soundtrack.pause();
   }
};

//spaceface.detectMobile();
//if(spaceface.mobile)
//{
//   //mobile specific stuff - touch handler etc
//   spaceface.instructionsElement = document.getElementById('spaceface-mobile-instructions');
//   spaceface.addTouchEventHandlers();
//}
//
//snailBait.fitScreen();
//window.addEventListener("resize", spaceface.fitScreen());
//window.addEventListener("orientationchange", spaceface.fitScreen());